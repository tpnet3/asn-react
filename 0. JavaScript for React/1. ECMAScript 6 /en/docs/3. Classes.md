# ES6 classes

Javascript was not the language of **`classes`**, rather it used objects. But ECMAScript commitee decided to add **`class`** to javaScript to facilitate developers, coming from other programming languages.

### A Class definition

```js
class Person {
  constructor(name) {
    this.name = name;
  }

  hello() {
    return "Hello, I am " + this.name + ".";
  }
}
```

A class has an identifier, which we can use to create new objects using new ClassIdentifier().

```js
const john = new Person("John");
john.hello();
```

> "Hello, I am John."

When the object is initialized, the `constructor` method is called, with any parameters passed.

### Class Inheritance

A class can extend another class, and objects initialized using that class inherit all the methods of both classes.

```js
class Programmer extends Person {
  hello() {
    return super.hello() + " I am a programmer.";
  }
}

const john = new Programmer("John");
john.hello();
```

> "Hello, I am John. I am a programmer."

**`super()`** is here referencing `parent` class' (Person) method.

Let's see another example.

```js
class Car {
  constructor(type) {
    this.type = type;
  }

  carType() {
    return this.type;
  }
}

const bmw = new Car("BMW");
bmw.carType();
```

> "BMW"

Now it could be little bit new to you if you have not used OOP (object oriented programming) but `class` is just syntactic suger over JavaScript Prototypes.

For example:

```js
function Car(type) {
  this.type = type;
}

Car.prototype.carType = function carType() {
  return this.type;
};

const bmw = new Car("BMW");
bmw.carType();
```

Example code shows that, **`function Car`** is **Constructor** function (constructing everything). _prototype_ is just a property taht every function in JavaScript has and , as we saw above, it allows us to share methods across all instances of a function.

> If you did not understand, that's ok, its advanced, you will get know them soon. But if did understand. YOU ROCK IT!

---

Challenge:

Extend your **`Car`** class with **`Ford`** class and use **`super`** method (parent classes method) by **`+ ' is better car.'`**
