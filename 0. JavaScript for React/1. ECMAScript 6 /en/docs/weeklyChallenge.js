window.addEventListener("load", function() {
  const root = document.getElementById("root");
  root.childNodes.forEach(node => node.remove());

  const listElement = document.createElement("div");
  const inputElement = document.createElement("input");

  const addButton = document.createElement("button");
  addButton.textContent = "Add";

  addButton.addEventListener("click", () => {
    const itemElement = document.createElement("div");

    const textNode = document.createTextNode(inputElement.value);

    const deleteButton = document.createElement("button");
    deleteButton.textContent = "Delete";
    deleteButton.addEventListener("click", () => {
      itemElement.remove();
    });

    itemElement.appendChild(textNode);
    itemElement.appendChild(deleteButton);

    listElement.appendChild(itemElement);
    inputElement.value = "";
  });

  root.appendChild(inputElement);
  root.appendChild(addButton);
  root.appendChild(listElement);
});

class TodoApp {
  constructor() {
    // Shortcuts to DOM Elements
    this.rootContainer = document.getElementById("root");

    // Creating childNodes
    this.listElement = document.createElement("div");
    this.inputElement = document.createElement("input");
    this.addButton = document.createElement("button");
    this.deleteButton = document.createElement("button");
    this.itemElement = document.createElement("div");

    this.addButton.textContent = "Add";
    // Method bindings
    this.addButton.addEventListener("click", this.saveTodo.bind(this));
    this.deleteButton.addEventListener("click", this.deleteTodo.bind(this));
  }

  saveTodo() {
    const item = this.listElement;
    const textNode = document.createTextNode(this.inputElement.value);
    const delButton = (this.deleteButton.textContent = `Delete`);

    delButton.addEventListener("click", this.deleteTodo(item));

    item.appendChild(textNode);
    item.appendChild(delButton);

    const rootElement = this.listElement;
    rootElement.appendChild(item);

    this.inputElement.value = "";
  }

  deleteTodo(item) {
    item.remove();
  }
}

window.addEventListener("load", () => new TodoApp());
