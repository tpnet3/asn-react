# ES6 클래스

자바스크립트는 클래스를 사용하지 않았었습니다. 그 것은 객체를 사용했었습니다. 하지만 ECMAScript 기어자들은 `class` 를 추가했고 다른 프로그래밍 언어의 개발자들을 좀더 편하게 만들었습니다.

## Class 가 무엇인가요?

클래스는 객체지향 프로그래밍에서 중요한 개념 중 하나입니다. 클래스는 관련된 변수나 함수들을 모아놓은 집단과 비슷한 개념이고, 똑같은 변수나 함수가 모여있는 집단을 서로 영향을 받지 않도록 복제하여 재사용할 수 있도록 도와줍니다.

```js
// 클래스 선언, 이것은 상자로도 비유됩니다
class Person {

  // 클래스는 초기 설정된 값을 바탕으로 복제해 재사용 할 수 있고, 이렇게 복제된 것을 인스턴스라고도 부릅니다. 인스턴스화 시킬 때 초기 값을 인자로 전달해줄 수 있습니다.
  constructor(name) {

    // this 는 만들어진 인스턴스 자기자신을 가르키며, this.name 은 인스턴스 변수입니다.
    this.name = name;
  }

  // 이 것은 클래스 내에 사용자가 정의한 인스턴스 함수입니다.
  hello() {
    return "Hello, I am " + this.name + ".";
  }
}
```

클래스는 `new` 키워드를 사용해서 인스턴스화 시킬 수 있습니다. 아래의 예제를 보세요.

```js
// Person 클래스를 인스턴스화 시킵니다.
const john = new Person("John");
john.hello(); // => "Hello, I am John."

// Person 클래스를 인스턴스화 시킵니다.
const smith = new Person("Smith");
smith.hello(); // => "Hello, I am Smith."
```

클래스를 인스턴스화 시킬 때 `constructor` 메소드가 실행되고, `new` 키워드를 통해 인스턴스화 시킬때 설정한 모든 매개변수가 전달됩니다. 메소드 안에서 `this` 는 현재의 인스턴스 자기자신을 가르킵니다.

## Class 상속

클래스는 다른 클래스를 바탕으로 확장시킬 수 있습니다. 아래 예제를 보세요.

```js
class Programmer extends Person {
  hello() {
    return super.hello() + " I am a programmer.";
  }
}

const john = new Programmer("John");
john.hello(); // => "Hello, I am John. I am a programmer."
```

`Programmer` 클래스는 `Person` 클래스를 상속받았습니다. 이에 따라 `Programmer` 클래스에서 `Person` 클래스의 변수나 함수를 사용할 수 있게 되었습니다. 이 것은 똑같은 규칙의 클래스를 여러개 만들어야 할 때 유용하게 사용됩니다.

`super` 는 상속받은 원래의 클래스를 가르킵니다. 즉, 위 예제 코드에서 `Person` 를 가르키고 있습니다.

`constructor` 는 클래스기 인스턴스화 될 때 호출되는 함수이며, 이 함수 내에서 `super(args)` 를 호출할 경우, 부모 클래스를 인스턴스화 시킬 때 인자를 전달하는 것과 같습니다. 즉, `new Person(args)` 을 통해 인자를 전달하는 것과 같은 업무를 수행합니다.

또 다른 예제를 보세요.

```js
class Car {
  constructor(type) {
    this.type = type;
  }

  carType() {
    return this.type;
  }
}

const bmw = new Car("BMW");
bmw.carType(); // => "BMW"
```

## 클래스는 사실 함수의 prototype 을 사용한 것입니다.

> 이 섹션은 이해하지 못했더라도 괜찮습니다. 현대의 자바스크립트에서 prototype 을 직접 사용하는 경우는 거의 없습니다.

아래 코드를 보세요. 이 것은 클래스가 아니지만, 자바스크립트에서는 함수로도 클래스의 개념을 활용할 수 있습니다. 엄밀히 말해, 현재의 클래스 기능은 여기서 설명하는 함수의 프로토타입 기능으로 구현되어 있습니다.

```js
function Car(type) {
  this.type = type;
}

Car.prototype.carType = function carType() {
  return this.type;
};

const bmw = new Car("BMW");
bmw.carType();
```

함수를 `new` 키워드를 활용해 인스턴스화 시킬 수 있으며, `Car.prototype` 에서 볼 수 있듯이, `prototype` 을 사용할 수 있습니다. 이 `prototype` 객체는 인스턴스화 시킬 때 사용되는 변수입니다. 위 예제에서는 `carType` 이라는 인스턴스 변수를 함수로 선언하였습니다.

## 도전 과제

여기, Car 클래스가 있습니다.

```js
class Car {
  constructor(type) {
    this.type = type;
  }

  carType() {
    return this.type;
  }
}
```

1. 새로운 클래스 `Ford` 를 만들고, `Car` 클래스를 상속받아 확장시키세요.

2. 새로 만든 클래스 `Ford` 에서 새로운 인스턴스 함수 `getRating()` 을 만들고, 부모 클래스에서 `super.carType()` 함수의 결과 값을 반환시키세요.
