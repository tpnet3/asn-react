# Destructuring 이 무엇인가요?

ES6 에서의 destructuring 은 객체의 값을 좀 더 쉽게 추출하여 사용할 수 있도록 도와주는 기능입니다.

## ES5 에서 객체안의 값을 추출하기

```js
const person = {
  firstName: "Tom",
  lastName: "Cruise",
  actor: true,
  age: 54
};
```

위 코드에서 객체의 값을 사용하기 위해서는 아래 코드 처럼 사용해야합니다.

```js
console.log(person.firstName, person.lastName);
// => 'Tom', 'Cruise'
```

또한, 객체 안의 값을 새로운 변수 명으로 사용하려면 아래처럼 변수를 다시 선언해줘야 합니다.

```js
const firstName = person.firstName;
const lastName = person.lastName;
console.log(firstName, lastName);
// => 'Tom', 'Cruise'
```

그것은 쉽지만, 매우 많은 변수들이 담겨있을 때에는 조잡해 보일 수 있습니다. 우리가 이번 시간에 배울 ES6 의 `Destructuring Assignments` 에서는 이 것을 좀 더 직관적으로 보여줄 수 있습니다.

## ES6 의 Destructuring

여기, 똑같은 **`person`** 객체를 작성했고, 위 코드와 똑같은 작업을 수행하는 코드를 ES6 문법을 활용하여 작성했습니다. 아래 코드를 보세요.

```js
const person = {
  firstName: "Tom",
  lastName: "Cruise",
  actor: true,
  age: 54
};

const {
  firstName,
  lastName
} = person;
// 위 코드는 아래 코드와 같은 의미입니다.
// const {
//   firstName: firstName,
//   lastName: lastName
// } = person;

console.log(firstName, lastName);
// => 'Tom', 'Cruise'
```

좀 더 깔끔하게 변수를 사용하기 위해서 매번 다시 선언해주지 않아도 됩니다. 위 코드에서 볼 수 있듯이, 변수 명을 지정하는 부분에 객체를 지정하고, 실제 객체의 키 값을 입력하면 그 값이 자동으로 변수로 매칭됩니다.

하지만 키 값이 자동으로 매칭되기 때문에, 다른 이름으로 바꿔줘야할 경우가 있을 것입니다. 그 때에는 아래 코드와 같이 사용되는 변수 명을 바꿔줄 수 있습니다.

```js
const person = {
  firstName: "Tom",
  lastName: "Cruise",
  actor: true,
  age: 54
};

const {
  // person 객체 안의 firstName 값은 tomFirstName 라는 변수로 매칭됩니다.
  firstName: tomFirstName,

  // person 객체 안의 lastName 값은 tomLastName 라는 변수로 매칭됩니다.
  lastName: tomLastName
} = person;

console.log(tomFirstName, tomLastName);
// => 'Tom', 'Cruise'
```

## 도전 과제

```js
const person = {
  color: 'red',
  type: 'BMW',
  year: '2018',
  price: '35.000$'
};
```

위 코드와 같은 객체가 있습니다. 이 객체에서 `Destructuring` 기능을 사용해 모든 객체의 내용을 `console.log` 로 출력해보세요.
