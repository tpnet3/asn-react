## Adding a new element

> In this section, you'll learn how to create a new element in JavaScript and add it as a child element of another element.

## `document.createElement()` 

Creating a new element is similar to using `document.createTextNode()`. The difference is that you must pass the tag name as an argument to `createElement()`. For example, you need to pass HTML tags like `div`,`p`.

Just like this:

```js
// From html DOM-Tree we will bring element with id name of root
const rootElement = document.getElementById("root");

// To the function to make div element we pass div as an argument
const divElement = document.createElement("div");
// To div we add text
divElement.textContent = "Hello, Element";

// To rootElement we add newly created div element as a child element
rootElement.appendChild(divElement)
```

This code creates an element with a `div` tag that contains the text node `"Hello, Element"`, and adds it as a child element to an element with the id of `root`.

When you want to create a new element, you can use `document.createElement()`. This is an element, so it is the same element that was imported into `document.getElementById()`. In other words, you can modify variables such as `divElement.style.backgroundColor` which can change the font size or change the background color.

The following code is an example of changing the style:

```js
// Create an element of the div tag.
const divElement = document.createElement("div");

// Change the font size for the next node in the element to `24px`.
divElement.style.fontSize = '24px';

// Insert '"Hello, Element"' text inside the text node
divElement.textContent = "Hello, Element";
```

## Article Challenge 

1. Get an element with the name `root`.

2. Create an element of the `p` tag, add `Hello, Element"`to the text node, and then add it to the element with the id of `root`.