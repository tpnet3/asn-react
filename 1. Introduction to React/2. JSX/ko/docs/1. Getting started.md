# 디렉토리 구조

> 첫번째 섹션에서 **`hello world`** 를 실행시켰고, 아래와 같은 디렉토리 구조를 봤을 것입니다. 이 섹션에서 각각의 디렉토리들이 어떤 역할을 하고 있는지 알아보겠습니다.

```bash
├- project  # 프로젝트 폴더
│  ├─ .cache  # 빠른 빌드를 위해 캐싱된 파일들이 퐇마된 폴더
│  ├─ dist  # 빌드된 파일이 포함된 폴더
│  ├─ node_modules  # 노드 모듈 파일이 포함된 폴더
│  ├─ public  # 정적 들이
│     └─ index.html  # 처음 가져올 정적 인덱스 파일
│  ├─ src  # 소스 폴더
│    └─ index.js  # 처음 실행될 React 소스파일
│  ├─ .gitignore  # Git 에 제외할 파일들이 퐇마된 설정 파일
│  ├─ package-lock.json  # Nodejs 패키지 매니저에 의한 관리 파일
│  └─ package.json  # Nodejs 패키지 매니저 설정 파일
```

이것이 무엇인지 혼란스럽더라도, 걱정하지마세요. 우리는 페이스북에 의해 개발된 [Create React App](https://facebook.github.io/create-react-app/) 툴을 사용했습니다. 이 툴은 Babel 과 Webpack 과 같은 복잡한 모든 환경 설정이 미리 구성되어 있는 싱글 페이지 리액트 앱 개발 도구입니다.

대부분의 폴더는 create-react-app 으로 설정되었고, 몇몇 파일들은 앱을 실행하는데 있어서 중요하기 떄문에 삭제하면 안됩니다.

모든 **`npm modules`** 은 **node_modules** 폴더에 저장되고, **dist** 폴더는 웹브라우저에서 작동할 수 있도록 빌드된 파일들이 담긴 폴더입니다. 파일을 **public** 폴더로 옮기면 이미지나 HTML 파일 등 정적 파일을 제공해줄수 있습니다. 우리는 개발을 위해 **src/** 폴더만 수정하면 됩니다.

> create-react-app 에 대해서 더 자세히 알고 싶으시다면 [create-react-app documentation](https://facebook.github.io/create-react-app/docs/folder-structure) 를 확인해보세요.

파일 안의 내용을 이해하기 어려워도 걱정하지 마세요. 다음 문서에서 그 것이 무엇을 의미하는지 설명할 것입니다. create-react-app 으로 초기 설정된 리액트 앱의 폴더 구조를 이해했다면, 다음 문서로 넘어가세요.
