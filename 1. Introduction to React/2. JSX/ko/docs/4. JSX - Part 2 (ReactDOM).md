# JSX - Part 3 (ReactDOM)

> 이번 섹션에서는 `Virtual DOM` 에 대한 개념을 설명하고, 이 개념이 적용된 `ReactDOM`에 대해 알아볼 것입니다.

## DOM 성능에 관한 문제

DOM 은 앞에서 설명한 것 처럼 트리 구조로 이루어져 있고 탐색하기 쉬워 사용성이 좋지만 이것은 DOM 을 효율적으로 관리하지 않을 경우 큰 규모로 이루어질수록 모든 트리 탐색을 반복하면서 성능이 매우 느려질 수 있습니다. 그래서 새로운 개념인 `Virtual DOM` 이 생겨났습니다.

## Virtual DOM 이란?

이전 방식에서 DOM 을 효율적으로 관리하지 못할 경우 DOM 을 수정하고, 추가하고, 삭제할 때마다 모든 DOM 트리를 탐색하며 매번 화면에 다시 그리는 과정이 반복되어 큰 규모의 웹어플리케이션에서는 성능 문제가 발생할 수 있습니다.

그래서 Virtual DOM 은 이전 DOM 트리와 새로운 DOM 트리를 한번에 탐색하고 모든 변경사항을 적용한 이후에 다시 그립니다.

## React DOM

> `ReactDOM` 은 `Virtual DOM` 의 개념이 적용된 라이브러리입니다. 이번 섹션에서는 `ReactDOM` 을 어떻게 사용할 수 있는지 알아보겠습니다.

여기, ReactDOM 을 이용해 엘리먼트를 랜더링하는 간단한 에제가 있습니다.

```js
import React from 'react';
import ReactDOM from 'react-dom';

// 아이템 엘리먼트를 생성합니다.
const itemElement = React.createElement(
  "li", // 엘리먼트의 태그
  { className: "list__item" }, // 엘리먼트의 속성 설정
  "List item" // 엘리먼트의 값
)

// 리스트 엘리먼트를 생성합니다.
const listElement = React.createElement(
  "ul", // 엘리먼트의 태그
  { className: "list" }, // 엘리먼트의 속성 설정
  itemElement // 엘리먼트의 값 (위에서 생성한 엘리먼트를 삽입)
);

// 필요한 모든 DOM 트리를 구성한 이후 한번에 렌더링
ReactDOM.render(list, document.body);
```

화면에 보이는 결과는 똑같지만, **React** 는 **`Virtual DOM`** 개념을 사용합니다. DOM 을 효율적으로 관리하지 않을 경우 매번 모든 DOM 트리를 랜더링하면서 성능 문제가 발생할 수 있지만, **`Virtual DOM`** 은 필요한 모든 DOM 트리를 구성한 이후 한번에 렌더링합니다.

이것이 **`React.DOM`** 이 수행하는 일입니다. 리액트 팀은 이 구문을 더 쉽고 간결하게 만들기 위해서 JSX 를 만들었습니다. JSX 는 HTML 과 비슷한 문법을 사용하고, HTML 의 기본적인 속성들도 비슷한 방법으로 사용할 수 있습니다.

여기 **`JSX`** 로 작성된 우리의 리스트가 있습니다.

```html
<ul className="list">
  <li className="list__item">
    List item one
  </li>
  <li className="list__item">
    List item two
  </li>
</ul>
```

일반적인 `HTML` 과 약간 다른 부분이 있어보이겠지만, 걱정하지 마세요. 우리는 다음 섹션에서 `JSX` 와 `HTML` 의 차이점에 대해 좀 더 자세히 알아볼 것입니다.

하지만 그 전에 우리가 지금까지 이해한 내용으로 완료해야할 도전과제가 있습니다.


## 도전과제

```bash
├─ project
│  └─ src
│     ├─ App.js
│     └─ index.js
```

1. **`src`** 폴더 안에 **`components`** 라는 이름의 새로운 폴더를 만드세요.

2. 새로 만든 **`components`** 폴더 안에 **`MyComponent.js`** 라는 이름의 새로운 파일을 만드세요.

3. **`src/App.js`** 파일의 모든 코드를 **`components/MyComponent.js`** 파일로 복사하세요.

4. **`components/MyComponent.js`** 파일에서 `class App extends Component` 으로 되어있는 클랙스 명을 `class MyComponent extends Component` 라고 바꾸세요.

5. 파일을 수출하기 위해 설정된 `export default App` 을 `export default MyComponent` 로 바꾸는 것도 잊지바세요.

6. 아래의 모든 코드를 **`src/App.js`** 파일로 복사하세요.

```js
import React, { Component } from 'react';
import MyComponent from './Components/MyComponent'
 
class App extends Component {
  render() {
    return (
      <MyComponent/>
    );
 }
}

export default App;
```

7. 모든 단계를 올바르게 완료했다면, `components/MyComponent.js` 의 내용을 `src/App.js` 에서 불러와 랜더링한 화면이 보여질 것입니다. 문제가 없다면 다음 단계로 진행하세요.