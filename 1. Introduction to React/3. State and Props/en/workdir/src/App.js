import React, { Component } from "react";
import MyProps from "./challenge/MyProps";
import Counter from "./challenge/MyStates";
import MyPropsAndState from "./challenge/MyPropsAndState";
import States from "./challenge/States";

class App extends Component {
  render() {
    return (
      <div>
        <center>
          <h1>Welcome to BounceCode</h1>
          {/*<Counter />*/}
          {/* <MyPropsAndState /> */}
          {/*<MyProps />*/}
          {/*<States />*/}
        </center>
      </div>
    );
  }
}

export default App;
