import React, { Component } from "react";

// 1. create button functional component which says "press me"

// 3. create NameProp functional component and access the prop which parent passed, return that prop in h1 tag

const LastName = props => <h1>{props.lastName}</h1>;

class MyProps extends Component {
  render() {
    return (
      <div>
        <NameProp name="John" />
        <LastName lastName="Denver" />
        {/* 2. return your Button component here */}
      </div>
    );
  }
}

export default MyProps;
