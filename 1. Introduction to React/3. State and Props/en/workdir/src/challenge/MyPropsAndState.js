import React from "react";

const ChildComponent = props => (
  <div>
    <h1>{/*access props which is passed by parent*/}</h1>
  </div>
);

class MyPropsAndState extends React.Component {
  state = {
    // create text state which says "this is component state"
  };

  render() {
    return (
      <div>
        <div>{this.state.text}</div>
        <ChildComponent text="This is props" />
      </div>
    );
  }
}

export default MyPropsAndState;
