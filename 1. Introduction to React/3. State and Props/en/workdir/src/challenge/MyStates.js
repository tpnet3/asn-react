import React from "react";

class Counter extends React.Component {
  state = {
    count: 0
  };

  incrementCount = () => {
    this.setState({
      count: this.state.count + 1
    });
  };

  render() {
    return (
      <button onClick={this.incrementCount}>
        <div>{this.state.count}</div>
      </button>
    );
  }
}

export default Counter;
