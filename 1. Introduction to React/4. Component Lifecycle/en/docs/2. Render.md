### render ()

After the class has been initialized through the constructor (), render () is called. This function is only present in the React component, and must be present in the React component. This is used to return a JSX to create a DOM tree to display on the screen.

In this method, all data which is sended by parent can be retrieved via **`this.props`**, and this component state data can be retrieved using **`this.state`**. Look at the example code below.

```js
import React, { Component } from "react";

class MyComponent extends Component {
  render() {
    // Get `props` through` this.props`.
    // `props` is the object that contains the data you set.
    const props = this.props; // Get `state` through` this.state`. // `state` is the object that contains the data set by the user.

    const state = this.state.customState; // Because `JSX` can not return an object directly, we converted it to a string using` JSON.stringify () `.

    return (
      <div>
                <h1> {JSON.stringify(props)} </h1>
                <h1> {JSON.stringify(state)} </h1>
              
      </div>
    );
  }
}

export default MyComponent;
```

Challenge:

inside your **`src/challenge/MyRender.js`**, there is one challenge try to finish it. And make sure to uncomment the **`MyRender`** import file in **`App.js`**.
