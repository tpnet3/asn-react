import React, { Component } from "react";
import MyConstructor from "./challenge/MyConstructor";
import MyRender from "./challenge/MyRender";

class App extends Component {
  render() {
    return (
      <div>
        <center>
          <h1>Welcome to BounceCode</h1>
          {/*<MyConstructor />*/}
          {/*<MyRender />*/}
        </center>
      </div>
    );
  }
}

export default App;
