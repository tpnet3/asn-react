import React, { Component } from "react";

class MyConstructor extends Component {
  constructor() {
    super();
    // finish the code and make ON, OFF switch
  }

  handleClick = () => {
    this.setState(prevState => ({
      isLightOn: !prevState.isLightOn
    }));
  };

  render() {
    return (
      <button onClick={this.handleClick}>
        {this.state.isLightOn ? "ON" : "OFF"}
      </button>
    );
  }
}

export default MyConstructor;
