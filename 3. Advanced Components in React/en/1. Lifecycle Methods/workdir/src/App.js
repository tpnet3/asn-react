import React, { Component } from "react";
import shouldComponentUpdate from "./challenge/shouldComponentUpdate";
import WillUnmount from "./challenge/WillUnmount";
import Unmount from "./example/unmount";
import DidMount from "./challenge/DidMount";
class App extends Component {
  render() {
    return (
      <div>
        <center>
          <h1>Welcome to BounceCode</h1>

          <h3>Below challange codes</h3>
          {/*<DidMount/> */}
          {/*<shouldComponentUpdate />*/}
          {/*<WillUnmount />*/}

          <h3>Below example codes</h3>
          {/*<Unmount />*/}
        </center>
      </div>
    );
  }
}

export default App;
