import React, { Component } from "react";

class Form extends Component {
  onFormSubmit = evt => {
    evt.preventDefault();
    console.log(this.refs.name.value);
  };

  render() {
    return (
      <div>
        <h1>Sign Up Sheet</h1>

        <form onSubmit={this.onFormSubmit}>
          <input type="text" placeholder="Name" ref="name" />

          <input type="submit" />
        </form>
      </div>
    );
  }
}

export default Form;
