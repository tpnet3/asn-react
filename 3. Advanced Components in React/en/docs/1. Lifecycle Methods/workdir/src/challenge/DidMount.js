import React, { Component } from "react";

class DidMount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "Jordan Belfort"
    };
  }

  getData() {
    setTimeout(() => {
      this.setState({
        data: "Hello WallStreet"
      });
    }, 1000);
  }

  // 1. Use componentDidMount() to call getData();

  render() {
    return <div>{this.state.data}</div>;
  }
}

export default DidMount;
