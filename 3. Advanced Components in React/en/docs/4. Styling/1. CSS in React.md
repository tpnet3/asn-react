# Styling in React

There are multiple ways of styling in React. In these sections we will see them and use them.

> You need to have basic CSS knowledge to continue.

### Importing CSS stylesheets

This way already should be familiar for you as you have many times used external CSS stylesheets in your HTML files. we will use same technique.

Open `src/App.js`

```js
import "./App.css";
```

this is how you import your external CSS Stylesheets. The only difference is you do not use `class` instead you have to use `className` to validate your tags in order to give them a unique style in your external stylesheet. As `class` keyword is already is used by JavaScript.

Challenge!!!

This is the CSS stylesheet file inside `src/challenge/externalStyle/externalStyle.css` file, import this file in your `src/challenge/externalStyle/externalStyle.js` file. And finish additional tasks inside `src/challenge/externalStyle/externalStyle.js` file.

```css
.myButton {
  font-size: 1em;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
}
```
