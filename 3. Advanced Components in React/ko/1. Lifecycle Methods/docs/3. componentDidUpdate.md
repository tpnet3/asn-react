# ComponentDidUpdate()

![componentLifecycle](https://cl.ly/8d79cb5f9e0b/download/Group%2525203.png)

이 튜토리얼을 잘 따라왔다면, 렌더링 되는 시점 (컴포넌트가 마운트 되는 시점) 에 대해 이미 알고 있을 것입니다.

> 마운팅은 컴포넌트를 DOM 엘리먼트로 만들고 DOM-API 를 사용해 DOM-Tree 를 변경시키는 것을 의미합니다.
> 다시 한번 복습하고 싶다면, **intro to React** 코스의 **DOM API** 섹션에서 이 것을 연습할 수 있습니다.

- constructor()
- render()
- componentDidMount()

### Updating

우리의 컴포넌트가 DOM 에 **_mounted_** 되었거나 **_inserted_** 된 이후에 `props` 나 `state` 가 변경된다면, 먼저 `shouldComponentUpdate` 가 호출될 것입니다. 만약 이 메소드가 클래스에 정의되어있지 않거나, `true` 를 반환한다면, `render()` 를 실행하고, 그 이후에 렌더링이 완료되면 여기서 설명할 `componentDidUpdate` 가 호출됩니다.

### Use Cases

**`componentDidMount`** 와 비슷합니다, 하지만 이 메소드는 `props` 나 `state` 의 변화에 따라 렌더링 될때마다 호출됩니다. 공식 문서에서는 이 것은 주로 상태값에 따라 데이터를 가져와야 할 때 유용하게 사용될 것이라고 설명하고 있습니다.

```js
componentDidUpdate(prevProps) {
  // Typical usage (don't forget to compare props):
  if (this.props.userID !== prevProps.userID) {
    this.fetchData(this.props.userID);
  }
}
```

위 코드는 현재의 `this.props.userID` 값과 이전의 `prevProps.userID` 값이 다를 때 데이터를 다시 가져오는 코드입니다. 반드시 조건문을 이용해 업데이트해야할 상황을 제어해줘야 한다는 것을 잊지 마세요. 그렇지 않으면 이 코드는 무한 루프에 빠질 것입니다.

```js
componentDidUpdate(prevState) {
  // Typical usage (don't forget to compare props):
  if (prevState.counter !== this.state.counter) {
    this.setState({
      counter: this.state.counter + 1
    });
  }
}
```

이코드는 `static getDerivedStateFromProps(props, state)` 메소드를 활용해 더 효율적인 코드로 만들 수 있습니다. 이 메소드는 잘 사용되지 않지만, `componentDidUpdate(prevState)` 메소드에서 상태값을 바로 수정하고 싶을 때 유용합니다. 만약 `componentDidUpdate(prevState)` 에서 `this.setState` 를 사용해 상태 값을 수정한다면, 이 코드는 화면을 2번 렌더링 하게 됩니다. 하지만 `static getDerivedStateFromProps(props, state)` 는 화면을 렌더링 하기 전에 호출되기 때문에 더 낫습니다. 다만 `static getDerivedStateFromProps(props, state)` 는 **컴포넌트가 마운팅 되는 시점** 과 **컴포넌트가 업데이트되는 시점** 두 경우에 호출됩니다. `componentDidUpdate(prevState)` 는 **컴포넌트가 업데이트되는 시점** 에만 호출된다는 점에서 약간 다른점이 있으니 사용시 유의해야합니다.

그래서, `componentDidUpdate(prevState)` 로 구현한 위 코드는 아래 코드와 같이`static getDerivedStateFromProps(props, state)` 메소드에서 구현할 수 있습니다. 이 메소드의 반환 값은 변경할 `state` 객체입니다. 아무것도 업데이트할 항목이 없다면 `null` 을 반환하면 됩니다.

> 일반적인 메소드는 `new MyApp()` 과 같은 방식으로 `new` 키워드를 활용해 인스턴스화 시킨 이후에 호출해야합니다. 하지만 `static` 키워드는 new 키워드를 활용해 인스턴스화 시키지 않은 상태에서도 호출할 수 있습니다. 예를들어, `new MyApp()` 없이 `MyApp.method()` 형태로 호출할 수 있습니다. 메소드 내에서 `this` 를 사용해야하거나 인스턴스 메소드로 만들어야 할 이유가 없다면 `static` 키워드를 사용해 메소드를 정의하는 것이 더 낫습니다. 왜냐하면 `static` 키워드로 선언된 메소드는 같은 클래스의 모든 인스턴스들과 메모리를 공유하여 사용하기 떄문입니다.

```js
static getDerivedStateFromProps(props, state) {
  return {
    counter: state.counter + 1
  }
}
```

이제 위 코드를 통해 `this.state.counter` 의 값은 초기값보다 `1` 증가된 상태에서 렌더링될 것입니다.

## 도전 과제

1. `src/challenge/DidUpdate.js` 파일에서 `componentDidUpdate(prevState)` 메소드를 구현하세요.

2. `this.state.id` 의 값이 이전 상태값과 다를 때에만 `this.fetchTest()` 를 호출하도록 구현하세요.

3. `this.fetchTest()` 의 반환값은 `Promise` 입니다. `this.fetchTest().than((result) => this.setState({data: result}))` 을 사용해 `this.state.data` 의 값을 `this.fetchTest()` 의 반환 값으로 업데이트하세요.