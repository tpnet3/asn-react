# componentWillUnmount()

컴포넌트 라이프 사이클의 마지막까지 왔습니다!

- mount
- update
- **`unmount`**

이 메소드는 컴포넌트가 돔에서 제거(unmounted)되기 직전에 호출됩니다.

이 메소드에서는 `setState()` 를 호출할 수 없고, 이 컴포넌트는 곧 제거될 것이기 때문에 다시 랜더링되지 않습니다. 이 메소드는 제거되기 직전에 한번만 호출됩니다.

`componentWillUnmount` 는 테이블 위의 물건들을 치우는 것과 비슷합니다. 테이블 위에 어려 물건들을 올려놓았다면, 우리는 테이블을 치우기 전에 테이블 위의 물건들을 정리해야하고, 그렇지 않으면 이 것은 엉망이 될 것입니다. 예를 들어 만약, `componentDidMount()` 에서 이벤트 리스닝을 설정했다면, 우리는 컴포넌트를 제거하기 전에 이벤트 리스닝을 제거해야합니다, 그렇지 않으면, 이 이벤트 리스닝이 메모리 상에서 없어지지 않고 계속 축척되어 메모리 누수가 발생합니다.

```js
class Timer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      time: 0
    };
  }

  componentDidMount() {
    this.interval = setInterval(this.onTick, 1000);
  }

  onTick = () => {
    console.log("Timer: " + this.state.time);

    this.setState(state => ({ time: state.time + 1 }));
  };

  componentWillUnmount() {
    // Remove this to see warning.
    clearInterval(this.interval);
  }

  render() {
    return <div>{this.state.time}</div>;
  }
}
```

`setInterval` 는 특정 함수를 x 초 마다 무한히 반복실행하는 함수입니다. 위 예제 코드는 `onTick` 함수를 1초마다 무한히 실행시키고 있습니다. 이 때, 만약 이 컴포넌트가 제거된다면, 함수가 더이상 실행되지 않도록 `clearInterval` 를 사용해 초기화시켜줘야 할 것입니다. 위 에서 볼 수 있듯이 `componentWillUnmount()` 에서 이를 해결할 수 있습니다.

> 전체 예제를 보고 싶다면, `src/examples/unmount.js` 파일을 열어보세요..
> 파일을 실행하고 테스트해보고 싶다면, `src/App.js` 파일의 `<Unmount/>` 부분의 주석을 제거하세요.

## 도전 과제

1. `src/challenge/WillUnmount.js` 파일에서 `componentWillUnmount()` 메소드를 구현하세요.

2. 이 컴포넌트는 `componentDidMount()` 에서 `setInterval()` 를 이용해 1초마다 영원히 컴포넌트를 업데이트하고 있습니다. `componentWillUnmount()` 를 활용해 컴포넌트가 DOM 에서 사라지기 직전에 `setInterval()` 도 더이상 작동하지 않도록 `clearInterval(this.interval)` 를 호출하세요.
