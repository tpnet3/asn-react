import React, { Component } from "react";
import DidMount from "./challenge/DidMount";
import shouldComponentUpdate from "./challenge/shouldComponentUpdate";
import getDerivedStateFromProps from "./challenge/getDerivedStateFromProps";
import WillUnmount from "./challenge/WillUnmount";
import Unmount from "./example/unmount";

class App extends Component {
  render() {
    return (
      <div>
        <center>
          <h1>Welcome to BounceCode</h1>

          <h3>Below challange codes</h3>
          {/*<DidMount />*/}
          {/*<shouldComponentUpdate />*/}
          {/*<getDerivedStateFromProps />*/}
          {/*<WillUnmount />*/}

          <h3>Below example codes</h3>
          {/*<Unmount />*/}
        </center>
      </div>
    );
  }
}

export default App;
