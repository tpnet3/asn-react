import React, { Component } from "react";

class DidMount extends Component {
  constructor(props) {
    super(props);

    this.getData = this.getData.bind(this);
    
    this.state = {
      data: "Jordan Belfort"
    };
  }

  getData() {
    setTimeout(() => {
      this.setState({
        data: "Hello WallStreet"
      });
    }, 1000);
  }

  // 1. Use componentDidMount() to call getData();

  render() {
    return (
      <div>
        <div>componentDidMount</div>
        <div>{this.state.data}</div>
      </div>
    )
  }
}

export default DidMount;
