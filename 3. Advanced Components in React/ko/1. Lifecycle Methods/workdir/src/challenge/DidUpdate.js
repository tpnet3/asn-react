import React, { Component } from "react";

class DidUpdate extends Component {
  constructor(props) {
    super(props);

    this.fetchTest = this.fetchTest.bind(this);
    
    this.state = {
      id: 0,
      data: "Please click the button"
    };
  }

  fetchTest() {
    return Promise.resolve((resolve, reject) => {
      setTimeout(() => {
        resolve('updated: ' + this.state.id)
      }, 1000);
    })
  }

  // 1. Use componentDidUpdate() to call fetchTest();

  render() {
    return (
      <div>
        <div>componentDidUpdate</div>
        <div>{this.state.data}</div>
        <div><button onClick={() => this.setState({id: Date.now()})}>Click to update</button></div>
      </div>
    )
  }
}

export default DidUpdate;
