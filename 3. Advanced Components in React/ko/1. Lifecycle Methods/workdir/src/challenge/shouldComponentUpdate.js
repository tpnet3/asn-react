import React, { Component } from "react";

class shouldComponentUpdate extends Component {
  constructor() {
    super();
    this.state = {
      value: true,
      countOfClicks: 0
    };
    this.pickRandom = this.pickRandom.bind(this);
  }

  pickRandom() {
    this.setState({
      value: Math.random() > 0.5, // randomly picks true or false
      countOfClicks: this.state.countOfClicks + 1
    });
  }

  // 1. write shouldComponentUpdate() to make component update or not.

  render() {
    return (
      <div>
        <div>shouldComponentUpdate</div>
        <div>
          <div>{this.state.value.toString()}</div>
          <div>Count of clicks: <b>{this.state.countOfClicks}</b></div>
          <div><button onClick={this.pickRandom}>Click to randomly select: true or false</button></div>
        </div>
      </div>
    );
  }
}

export default shouldComponentUpdate;
