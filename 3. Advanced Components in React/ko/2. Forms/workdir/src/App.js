import React, { Component } from "react";
import shouldComponentUpdate from "./challenge/shouldComponentUpdate";
import MyRender from "./challenge/MyRender";
import Unmount from "./example/unmount";

class App extends Component {
  render() {
    return (
      <div>
        <center>
          <h1>Welcome to BounceCode</h1>

          <h3>Below challange codes</h3>
          {/*<shouldComponentUpdate />*/}

          <h3>Below example codes</h3>
          {/*<Unmount />*/}
        </center>
      </div>
    );
  }
}

export default App;
