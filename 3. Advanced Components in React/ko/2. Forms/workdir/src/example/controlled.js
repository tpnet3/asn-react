import React, { Component } from "react";

class Controlled extends Component {
  state = {
    name: " "
  };

  onFormSubmit = evt => {
    evt.preventDefault();
    this.setState({ name: evt.target.value });
  };

  render() {
    return (
      <div>
        <h1>Sign Up Sheet</h1>

        <form onSubmit={this.onFormSubmit}>
          <input
            type="text"
            placeholder="Name"
            value={this.state.name}
            onChange={this.onFormSubmit}
          />

          <input type="submit" />
        </form>
      </div>
    );
  }
}

export default Controlled;
