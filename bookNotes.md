# Book Notes
Regardless of the method we used to define, React expects us to define the render() function.

> render() method is the only required method to be defined on a `ReactComponent`

### props are the function parameters

props are teh inputs to our components. If we think of our component as a function, we can think of the props as the arguments.

```js

<div>
  <Header headerText="Hello world"/>
</div>

```

**`<Header>`** element here is an instance of our **Header** component.


### PropTypes

```js

class MapComponent extends React.Component {
  static propTypes = {
    lat: PropTyeps.number,
    lng: PropTypes.number,
    zoom: PropTypes.number,
    place: PropTypes.object,
    markers: PropTypes.array,
  }
};
```

### getDefaultProps()

```js
class Counter extends React.Component {
  static defaultProps = {
    initialValue: 1
  };
}
```

Usage

```js
<Counter />
<Counter initialValue={1} />
```

If there is no value for the component now, we can use these components as an `initialValue`


### States 

states makes composing components more difficult that's why we better use few stateful components as possible.




### Props vs State

Props (short for "properties") and states are both js objects which hold data influence the output of render. The difference is **`props`** get passed to components(*similar to function parameters*) whereas **`states`**  managed within the component(*similar to variables declared within a function*).



# Unit Testing

